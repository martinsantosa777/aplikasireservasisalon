package comebackisreal.com.aplikasireservasisalon

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val fragmentManager = supportFragmentManager
    val fragmentTransaction = fragmentManager.beginTransaction()
    val HistoryFragment = comebackisreal.com.aplikasireservasisalon.HistoryFragment()
    val ProfileFragment = comebackisreal.com.aplikasireservasisalon.ProfileFragment()

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            //HairStyle Fragment
            R.id.navigation_home -> {
                return@OnNavigationItemSelectedListener true
            }
            //History Fragment
            R.id.navigation_dashboard -> {
                fragmentTransaction.replace(R.id.contextFrameLayout, HistoryFragment, HistoryFragment.javaClass.simpleName)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
}
